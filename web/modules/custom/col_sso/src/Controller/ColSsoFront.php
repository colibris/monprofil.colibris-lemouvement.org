<?php

namespace Drupal\col_sso\Controller;

use Drupal\Core\Controller\ControllerBase;
use Firebase\JWT\JWT;
use Symfony\Component\HttpFoundation\Request;

class ColSsoFront extends ControllerBase {
  public function frontpage(Request $request) {
    if (\Drupal::currentUser()->isAnonymous()) {
      return $this->redirect('cas_server.login');
    }
    else {
      // Check if user logs from 360 learning.
      if (isset($_REQUEST['goto']) && $_REQUEST['goto'] == '360learning') {
        $account = \Drupal\user\Entity\User::load(\Drupal::currentUser()->id());
        return col_sso_redirect_to_360_learning($account);
      }
      elseif (isset($_REQUEST['goto']) && $_REQUEST['goto'] == 'communaute_colibris_universite') {
        $account = \Drupal\user\Entity\User::load(\Drupal::currentUser()->id());
        return col_sso_redirect_to_communaute_colibris_universite($account);
      }
      elseif (isset($_REQUEST['goto']) && $_REQUEST['goto'] == 'humhub_dev_localhost') {
        $account = \Drupal\user\Entity\User::load(\Drupal::currentUser()->id());
        return col_sso_redirect_to_humhub_dev_localhost($account);
      }

      return $this->redirect('user.page');
    }
  }
}
