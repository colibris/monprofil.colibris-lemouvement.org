<?php

namespace Drupal\col_sso\EventSubscriber;

use Drupal\cas_server\Configuration\ConfigHelper;
use Drupal\cas_server\Event\CASAttributesAlterEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class CASAttributeAlterEventSubscriber implements EventSubscriberInterface
{

  /**
   * {@inheritdoc}
   */
    public function __construct(ConfigHelper $configHelper)
    {
        $this->configHelper = $configHelper;
    }

    /**
     * Registers the methods in this class that should be listeners.
     *
     * @return array
     *   An array of event listener definitions.
     */
    public static function getSubscribedEvents()
    {
        $events[CASAttributesAlterEvent::CAS_ATTRIBUTES_ALTER_EVENT][] = [
      'onCASAttributeAlter',
      50,
    ];
        return $events;
    }

    public function onCASAttributeAlter(CASAttributesAlterEvent $event)
    {
        $eventAttributes = [];
        $attributes = $this->configHelper->getAttributesForService($event->getTicket()->getService());
        if (!empty($attributes)) {
            foreach ($attributes as $attr) {
                foreach ($event->getUser()->get($attr)->getValue() as $value) {

          // Some properties (fields) might provide more than just a "value"
                    // field (ie. address). In those cases, encode them as we can't
                    // add sub-array here.
                    if (count($value) > 1) {
                        $eventAttributes[$attr] = json_encode($value);
                    } else {
                        $eventAttributes[$attr] = str_replace('&', 'et', $value['value']);
                    }
                }
            }
            $avatar = $event->getUser()->get('field_avatar');
            if (!empty($avatar) && isset($avatar->entity)) {
                $eventAttributes['field_avatar'] = $avatar->entity->createFileUrl(false);
            } else {
                $eventAttributes['field_avatar'] = '';
            }
        }
        $event->setAttributes($eventAttributes);
    }
}
