<?php

/**
 * This file is included very early. See autoload.files in composer.json and
 * https://getcomposer.org/doc/04-schema.md#files
 */

use Dotenv\Dotenv;
use Dotenv\Exception\InvalidPathException;

/**
 * Load any settings.sh file. See happyculture/combawa dependency.
 */
$settings = new Dotenv(__DIR__ . '/scripts', 'settings.sh');
try {
  $settings->load();
}
catch (InvalidPathException $e) {
  // Do nothing. This exception is going to be handled by combawa.
}

/**
 * Load any .env file. See /.env.example.
 */
$dotenv = new Dotenv(__DIR__);
try {
  $dotenv->overload();
}
catch (InvalidPathException $e) {
  // Do nothing. Production environments rarely use .env files.
}
