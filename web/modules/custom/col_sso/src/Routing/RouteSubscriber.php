<?php

namespace Drupal\col_sso\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Listens to the dynamic route events.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    // Set the CAS login page title.
    if ($route = $collection->get('cas_server.login')) {
      $route->setDefault('_title', 'Log in');
      $route->setDefault('_controller', '\Drupal\col_sso\Controller\ColSsoLogin::login');
    }
    // Limit access to the CAS logout link if the user is logged in.
    // It brings a more consistent link in the footer, that becomes only
    // accessible if the user is logged in.
    if ($route = $collection->get('cas_server.logout')) {
      // Remove free access to the route and replace it with the user is logged
      // in check.
      $requirements = $route->getRequirements();
      unset($requirements['_access']);
      $requirements['_user_is_logged_in'] = 'TRUE';
      $route->setRequirements($requirements);
      $route->setDefault('_controller', '\Drupal\col_sso\Controller\ColSsoLogin::logout');
    }
  }

}
