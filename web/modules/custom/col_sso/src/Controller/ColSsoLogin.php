<?php

/**
 * @file
 * Contains \Drupal\cas_server\Controller\UserActionController.
 */

namespace Drupal\col_sso\Controller;

use Drupal\cas_server\Controller\UserActionController;
use Drupal\cas_server\RedirectResponse;
use Drupal\Component\Utility\Crypt;
use Drupal\Core\Url;

/**
 * Class ColSsoLogin.
 */
class ColSsoLogin extends UserActionController {

  /**
   * Handles a page request for /cas/login.
   *
   * Full rewrite/copy-paste since we want to add some king of logic in the
   * middle of it.
   */
  public function login() {
    $this->killSwitch->trigger();
    $request = $this->requestStack->getCurrentRequest();
    $service = $request->query->has('service') ? $request->query->get('service') : NULL;

    // If we have a ticket, it is because we've already processed the form and
    // need to be redirected back to the service.
    if ($request->query->has('ticket') && $this->configHelper->checkServiceAgainstWhitelist($service)) {
      $url = Url::fromUri($service, ['query' => ['ticket' => $request->query->get('ticket')]]);
      return RedirectResponse::create($url->toString(), 302);
    }

    if ($request->query->has('gateway')) {
      if ($request->query->get('gateway') == 'false') {
        $gateway = FALSE;
      }
      else {
        $gateway = (bool)$request->query->get('gateway');
      }
    }
    else {
      $gateway = FALSE;
    }

    if ($request->query->has('renew')) {
      if ($request->query->get('renew') == 'false') {
        $renew = FALSE;
      }
      else {
        $renew = (bool)$request->query->get('renew');
      }
    }
    else {
      $renew = FALSE;
    }

    // Special case for our goto implementation to handle JWT redirection.
    if ($request->query->has('goto') && $this->userHasSingleSignOnSession(NULL)) {
      $url = \Drupal\Core\Url::fromRoute('<front>', ['goto' => $request->query->get('goto')])->toString();
      return new RedirectResponse($url);
    }
    // If no service, need to either show the login form (if not logged in),
    // or a simple page to logged in users explaining their state.
    if (is_null($service)) {
      if (!$this->userHasSingleSignOnSession(NULL)) {
        return \Drupal::formBuilder()->getForm('\Drupal\cas_server\Form\UserLogin', '');
      }
      // If the user just logged in, redirect him to the front page and display
      // a message to confirm him the action.
      else {
        $message = $this->generateLoggedInMessage();
        \Drupal::messenger()->addMessage($message);
        $url = \Drupal\Core\Url::fromRoute('<front>')->toString();
        return new RedirectResponse($url);
      }
    }

    // Check service against whitelist. If its not a valid service, display
    // a page to that effect.
    if (!$this->configHelper->checkServiceAgainstWhitelist($service)) {
      return $this->generateInvalidServiceMessage();
    }

    // If user has an active single sign on session and renew is not set,
    // generate a service ticket and redirect.
    if (!$renew && $this->userHasSingleSignOnSession($service)) {
      $st = $this->ticketFactory->createServiceTicket($service, FALSE);
      $url = Url::fromUri($service, ['query' => ['ticket' => $st->getId()]]);
      return RedirectResponse::create($url->toString(), 302);
    }
    
    // If gateway is set and user is not logged in, redirect them back to
    // service.
    if ($gateway && !$this->userHasSingleSignOnSession($service)) {
      return RedirectResponse::create($service, 302);
    }

    // Present the user with a login form.
    return \Drupal::formBuilder()->getForm('\Drupal\cas_server\Form\UserLogin', $service);

  }

  /**
   * Whether or not a user has a valid single sign on session for a given service.
   *
   * @param string $service
   *   The service to check for.
   *
   * @return bool
   */
  protected function userHasSingleSignOnSession($service) {
    if (!is_null($service) && !$this->configHelper->verifyServiceForSso($service)) {
      return FALSE;
    }

    if ($this->configHelper->shouldUseTicketGrantingTicket() && isset($_COOKIE['cas_tgc'])) {
      try {
        $tgt = $this->ticketStore->retrieveTicketGrantingTicket(urldecode($_COOKIE['cas_tgc']));
      }
      catch (TicketTypeException $e) {
        $this->logger->log($e->getMessage());
        return FALSE;
      }
      catch (TicketMissingException $e) {
        $this->logger->log("Ticket not found " . urldecode($_COOKIE['cas_tgc']));
        return FALSE;
      }

      if (REQUEST_TIME > $tgt->getExpirationTime()) {
        $this->ticketStore->deleteTicketGrantingTicket($tgt);
        return FALSE;
      }

      if ($this->account->id() != $tgt->getUid()) {
        return FALSE;
      }

      return TRUE;
    }
    elseif (!$this->configHelper->shouldUseTicketGrantingTicket() && !$this->account->isAnonymous()) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Markup for an invalid service message.
   *
   * @return array
   *   A renderable array.
   */
  protected function generateInvalidServiceMessage() {
    $output['header'] = ['#markup' => '<h2>' . $this->t('Invalid Service') . '</h2>'];
    $m = $this->configHelper->getInvalidServiceMessage();
    $message = $m ? $m : $this->t('You have not requested a valid service.');
    $output['message'] = ['#markup' => $message];

    return $output;
  }

  /**
   * Markup for logged in message.
   *
   * @return array
   *   A renderable array.
   */
  protected function generateLoggedInMessage() {
    $m = $this->configHelper->getLoggedInMessage();
    return $m ? $m : $this->t('You are logged in to CAS single sign on.');
  }

  /**
   * Handles a page request for /cas/logout.
   */
  public function logout() {
    $this->killSwitch->trigger();
    if (isset($_COOKIE['cas_tgc'])) {
      unset($_COOKIE['cas_tgc']);
      setcookie('cas_tgc', '', REQUEST_TIME - 3600, '/cas');
    }
    $session_id = $this->sessionManager->getId();
    $hashed_id = Crypt::hashBase64($session_id);
    $this->ticketStore->deleteTicketsBySession($hashed_id);
    $this->userLogout();

    // @TODO: Find out how to display the message, as is, it's not displayed
    // because of the cache probably.
    $message = $this->generateUserLogoutMessage();
    \Drupal::messenger()->addMessage($message);
    $url = \Drupal\Core\Url::fromRoute('<front>')->toString();
    return new RedirectResponse($url);
  }

  /**
   * Markup for logout message.
   *
   * @return array
   *   A renderable array.
   */
  protected function generateUserLogoutMessage() {
    $m = $this->configHelper->getUserLogoutMessage();
    return $m ? $m : $this->t('You have been logged out');
  }

  /**
   * Encapsulates user_logout.
   */
  protected function userLogout() {
    user_logout();
  }
}
