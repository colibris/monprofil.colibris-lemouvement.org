<?php

namespace Drupal\col_sso\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Url;

/**
 * Provides a block that expose actions on /user pages.
 *
 * @Block(
 *   id = "col_sso_user_actions",
 *   admin_label = @Translation("Colibris SSO user actions"),
 *   category = @Translation("Colibris")
 * )
 */
class ColSsoUserActions extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $actions = [
      'user.register' => [
        'title' => "Si vous n'avez pas de compte :",
        'link' => 'Créer un nouveau compte',
      ],
      'cas_server.login' => [
        'title' => 'Si vous êtes déjà inscrit :',
        'link' => 'Connectez-vous à votre espace',
      ],
    ];

    $build = [];
    $current_route_name = \Drupal::service('current_route_match')->getRouteName();
    foreach ($actions as $route_name => $content) {
      if ($route_name === $current_route_name) {
        continue;
      }

      $build = [
        '#type' => 'link',
        '#title' => $content['link'],
        '#url' => Url::fromRoute($route_name),
        '#options' => [
          'attributes' => [
            'class' => ['cta-button', 'lonely-button'],
          ],
        ],
      ];
    }

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    return Cache::mergeContexts(parent::getCacheContexts(), ['url.path', 'url.query_args']);
  }
}
