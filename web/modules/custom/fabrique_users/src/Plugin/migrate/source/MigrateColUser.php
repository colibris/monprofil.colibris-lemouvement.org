<?php

namespace Drupal\fabrique_users\Plugin\migrate\source;

use Drupal\migrate\Row;
use Drupal\user\Plugin\migrate\source\d7\User as MigrateUser;

/**
 * Drupal 7 user source from database.
 *
 * @MigrateSource(
 *   id = "MigrateColUser",
 *   source_module = "user"
 * )
 */
class MigrateColUser extends MigrateUser {

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    parent::prepareRow($row);
  }

}
