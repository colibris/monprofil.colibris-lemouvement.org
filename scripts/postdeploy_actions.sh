#!/bin/bash
# Action to run after the main and shared deployment actions.
# It can be useful to enable specific modules for instance.
# Available variables are defined in settings.sh.

# Return error codes if they happen.
set -e

PROFILE=colibris_sso
THEME=colibris_sso_theme

case $COMBAWA_ENV in
  dev)
    # Run migrations
    #$DRUSH cset geocoder.settings geocoder_presave_disabled 1
    #$DRUSH migrate:import d7_custom_d7_user --limit=50
    $DRUSH cset geocoder.settings geocoder_presave_disabled 0

    # Install col_install_once one time to create content. We don't have any
    # other option to create content and menu links (except using default
    # content) after making sure that the menu is imported.
    # Example: after running drush site-install, the node created in the
    # hook_install() are created but not the menu items. It doesn't make much
    # sense but it's the only viable option which is not too much time
    # consuming.
    #$DRUSH en col_install_once
    #$DRUSH pmu col_install_once

    # Compile CSS for development.
    (
      cd $WEBROOT/themes/custom/$THEME
      if [ "$COMBAWA_OFFLINE" == "0" ] ; then npm install; fi
      npm run build-dev
    )

    # Connect.
    $DRUSH uli
    ;;
  recette|preprod)
    # Run migrations
    #$DRUSH cset geocoder.settings geocoder_presave_disabled 1
    #$DRUSH migrate:import d7_custom_d7_user --feedback='1000 items'
    $DRUSH cset geocoder.settings geocoder_presave_disabled 0

    # Install col_install_once one time to create content. We don't have any
    # other option to create content and menu links (except using default
    # content) after making sure that the menu is imported.
    # Example: after running drush site-install, the node created in the
    # hook_install() are created but not the menu items. It doesn't make much
    # sense but it's the only viable option which is not too much time
    # consuming.
    #$DRUSH en col_install_once
    #$DRUSH pmu col_install_once

    # Update locale
    $DRUSH locale:check
    $DRUSH locale:update
    ;;
  prod)
    # Update locale
    $DRUSH locale:check
    $DRUSH locale:update
    ;;
  *)
    echo "Unknown environment: $COMBAWA_ENV. Please check your name."
    exit 1;
esac

