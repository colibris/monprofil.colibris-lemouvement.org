## Fabrique_users module

### Execute the migration

First, change the database configuration in the file `config/install/migrate_plus.migration_group.fabrique_drupal_7.yml`:
```database:
         driver: mysql
         username: drupal
         password: drupal
         host: localhost
         port: 3306
         database: colibris_lafabrique
         prefix: null
```

Then, enable this module:

```drush en fabrique_users```

Then to process the users importation, type:

```drush migrate-import fabrique_d7_user```

There is no progress message, you will just have "Processed X items" with X the number of users processed.
You have in detail the ones correctly imported (labelled as "created") and the one which already have an account
with the same mail in the destination server (labelled as "ignored").

By typing :
```drush migrate-messages fabrique_d7_user```
you can see in the messages, all the emails which already exists in the destination database.

### Others helpfull `drush` instructions

To see this migration status:
```drush migrate-status --group fabrique_drupal_7```

To rollback the migration process:
```drush migrate-rollback fabrique_d7_user```

To cancel an migration in process:
```drush migrate:reset-status fabrique_d7_user```

To import only 10 users (in case of testing):
```drush migrate-import fabrique_d7_user --limit=10```

If you modify the `migrate_plus.migration.fabrique_d7_user.yml`file, you can reload the modifications by only executing :
```drush config-import --partial --source=modules/custom/fabrique_users/config/install/```

You can reload completely the module by typing:
```
drush pm:uninstall fabrique_users
drush config-delete migrate_plus.migration.fabrique_d7_user
drush config-delete migrate_plus.migration_group.fabrique_drupal_7
drush en fabrique_users
```
