<?php

namespace Drupal\col_sso\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Main user controller.
 */
class ColSsoUser extends ControllerBase {

  /**
   * Redirect the "user/cancel" page to the cancel user page with current
   * user ID in it so we can use the "generic" link as a menu item.
   */
  public function cancelUser() {
    return $this->redirectUser('entity.user.cancel_form');
  }

  /**
   * Redirect the "user/edit" page to the cancel user page with current
   * user ID in it so we can use the "generic" link as a menu item.
   */
  public function editUser() {
    return $this->redirectUser('entity.user.edit_form');
  }

  private function redirectUser($route) {
    $uid = \Drupal::currentUser()->id();
    $url = Url::fromRoute($route, ['user' => $uid]);

    return new RedirectResponse($url->toString());
  }

}
