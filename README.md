# monprofil.colibris-lemouvement.org

Drupal 8 CAS serveur pour centraliser l'authentification sur les services colibris.

## Installation

Le projet utilise combawa https://github.com/Happyculture/combawa

Lors de la première installation il faut générer les fichiers d'environnement

`composer install`

puis

`drupal combawa:generate-environment`