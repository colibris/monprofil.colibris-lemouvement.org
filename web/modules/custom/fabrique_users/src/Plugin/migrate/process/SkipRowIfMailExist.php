<?php

namespace Drupal\fabrique_users\Plugin\migrate\process;

use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;
use Drupal\migrate\MigrateSkipRowException;

/**
* Skips processing the current row if an user already exists with the same mail.
*
* @code
*  process:
*    uid:
*      -
*        plugin: get
*        source: uid
*      -
*        plugin: skip_row_if_email_exist
* @endcode
*
* @see \Drupal\migrate\Plugin\MigrateProcessInterface
*
* @MigrateProcessPlugin(
*   id = "skip_row_if_mail_exist"
* )
*/
class SkipRowIfMailExist extends ProcessPluginBase
{

  function __construct($configuration, $plugin_id, $plugin_definition)
  {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property)
  {
    $ids = \Drupal::entityQuery('user')
      ->condition('mail', $row->getSourceProperty('mail'))
      ->execute();

    if (!empty($ids)) {
      throw new MigrateSkipRowException("Email'" . $row->getSourceProperty('mail') . "' already exists in destination database. The row is skipped");
    }
    return $value;
  }

}
